const SteamUser = require('steam-user')
const GlobalOffensive = require('globaloffensive')

const steamUser = new SteamUser()
const csgo = new GlobalOffensive(steamUser)

const config = require('./config')

steamUser.logOn({
	accountName: config.accountName,
	password: config.password,
})

steamUser.on('loggedOn', (details) => {
	console.log('Logged into SteamUser as ' + steamUser.steamID.getSteam3RenderedID())
	steamUser.setPersona(SteamUser.EPersonaState.Online)

	steamUser.gamesPlayed([730])
})

csgo.on('connectedToGC', () => {
	console.log('Connected')
	csgo.nameItem(config.nametag, config.item, config.name)
})

csgo.on('disconnectedFromGC', () => {
	console.log('Disconnected')
})

csgo.on('itemChanged', (oldItem, item) => {
	console.log(`Successfully renamed \n${JSON.stringify(item.custom_name)}`)
})
